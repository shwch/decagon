﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace DecagonProblem3
{
    class stringManipulation
    {
        
        public void GetInput()
        {
            string input;
            string userInput = string.Empty;
            do
            {
                Console.WriteLine("Enter String");
                input = Console.ReadLine();

                if (string.IsNullOrWhiteSpace(input))
                {
                    Console.WriteLine("Please enter a valid input string");

                }
                else
                {
                    Console.WriteLine("Enter choice 1:ChangeToUpperCase, 2:ChangeToLowerCase, 3:AddSpaces, 4:RemoveSpaces");
                   int choice = Convert.ToInt32(Console.ReadLine());


                    switch (choice)
                    {
                        case 1:
                            ChangeToUpperCase(input);
                            break;

                        case 2:
                            ChangeToLowerCase(input);
                            break;

                        case 3:
                            AddSpaces(input);
                            break;

                        case 4:
                            RemoveSpaces(input);
                            break;

                        default:
                            Console.WriteLine("Choice should be between 1 to 4");
                            break;

                    }

                    Console.WriteLine("Want to continue? Say yes or no");
                    userInput = Console.ReadLine().ToLower();
                }
                
               
                
            } while (userInput == "yes");


            Console.WriteLine("Thank you");
        }

         void ChangeToUpperCase(string input)
        {
            input = input.ToUpper();
            Console.WriteLine(input);
        }

        void ChangeToLowerCase(string input)
        {
            input = input.ToLower();
            Console.WriteLine(input);
        }

        void AddSpaces(string input)
        {
            StringBuilder builder = new StringBuilder(input.Length * 2);
            char[] inputLetters = input.ToCharArray();

            
            foreach (char letter in inputLetters)
            {
                builder.Append(letter);
                builder.Append(" ");
            }

            Console.WriteLine(builder);

        }

        void RemoveSpaces(string input)
        {
            input = input.Replace(" ", "");
            Console.WriteLine(input);

        }

    }
}
