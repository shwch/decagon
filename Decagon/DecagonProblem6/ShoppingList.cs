﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DecagonProblem6
{
    class ShoppingList
    {
        private List<string> mylist = new List<string>();

        public void SetItemList()
        {
            string shoppingList;
          

            Console.WriteLine("Enter shopping Items separating each with comma");
            shoppingList = Console.ReadLine();

            var results = shoppingList.Split(',').Where(x => x.Length > 3)
                                         .GroupBy(x => x)
                                         .Select(x => new { Count = x.Count(), Word = x.Key })
                                         .OrderByDescending(x => x.Count);

            
            Console.Write("Maximum Item shopped is: "+results.First().Word);
            Console.ReadLine();
            
        }
    }
}
