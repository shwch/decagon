﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DecagonProblem4
{
    class Translator
    {
        public void GetInput()
        {

            int choice;

            Console.WriteLine("Enter choice 1: Marathi to English \n choice 2: Hindi to English");
            choice = Convert.ToInt32(Console.ReadLine());

            switch (choice)
            {
                case 1: SetMarathiDictionary();
                    break;
                case 2: SetHindiDictionary();
                    break;

                default: Console.WriteLine("Invalid Choice entered");
                    break;
            }

           
           
            
        }

        public void SetMarathiDictionary()
        {
            string input;
            Console.WriteLine("Enter a string");
            input = Console.ReadLine().ToLower();
            Hashtable marathiHashtable = new Hashtable();
            marathiHashtable.Add("aai", "Mummy");
            marathiHashtable.Add("aag gaadi", "Train");
            marathiHashtable.Add("paani", "Water");
            marathiHashtable.Add("poli", "Indian Bread");
            GetEnglishTranslation(input,marathiHashtable);


        }
        
        public void SetHindiDictionary()
        {
            string input;
            Console.WriteLine("Enter a string");
            input = Console.ReadLine().ToLower();
            Hashtable hindiHashtable = new Hashtable();
            hindiHashtable.Add("maa","Mummy");
            hindiHashtable.Add("rail gaadi", "Train");
            hindiHashtable.Add("paani", "Water");
            hindiHashtable.Add("roti", "Indian Bread");
            GetEnglishTranslation(input, hindiHashtable);

        }
        private void GetEnglishTranslation(string wordEntered ,Hashtable languageHastable)
        {
            if (languageHastable.ContainsKey(wordEntered))
            {
               Console.Write(languageHastable[wordEntered].ToString());

            }
            else
            {
                Console.WriteLine("Word not present in dictionary.No English translation available");
             
            }

           

        }

    }
}
