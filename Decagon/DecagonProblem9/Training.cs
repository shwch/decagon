﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DecagonProblem9
{
    class Training
    {

      
     public int CalTrainingCost()
        {
            int numberOfPeople, groupSize, CostOfToken, CostOfGift;

            Console.Write("Enter number of people:");
            numberOfPeople = Convert.ToInt32(Console.ReadLine());

            Console.Write("Enter group size:");
            groupSize = Convert.ToInt32(Console.ReadLine());

            int peopleInGroup = numberOfPeople - (numberOfPeople%groupSize);

            Console.Write("Enter cost of token:");
            CostOfToken = Convert.ToInt32(Console.ReadLine());

            int totalTokenCost = peopleInGroup*CostOfToken;

            Console.Write("Enter cost of gift:");
            CostOfGift = Convert.ToInt32(Console.ReadLine());

            int totalGiftCost = CostOfGift*(numberOfPeople%groupSize);

            int totalTrainingCost = totalTokenCost + totalGiftCost;

            return totalTrainingCost;


        }

    }
}
