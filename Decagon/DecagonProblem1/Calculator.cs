﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace DecagonProblem1
{
    class Calculator
    {
      
        int unitNumber;
        int tensNumber;
      
        public void GetNumber()
        {
            string ans;

                do
                {
                    Console.WriteLine("Enter a two digit number");
                   int number = Convert.ToInt32(Console.ReadLine());


                    if (number != 0)
                    {
                         FetchOnesAndUnitDigits(number);

                        int addedSum = AddSum();
                        int newSum = Subtract(number,addedSum);

                       FetchOnesAndUnitDigits(newSum);
                       int sum = unitNumber + tensNumber;

                        Console.WriteLine("Sum of the Subtracted digit number :" + sum);
                        Console.ReadLine();

                    }
                    else
                    {
                        Console.WriteLine("Invalid number");

                    }

                    Console.WriteLine("Do you want to continue? say yes or no");
                    ans = Console.ReadLine().ToLower();


                } while (ans == "yes");
            
             
              Console.WriteLine("Thank you!!!");
            
           
            
        }

        public void FetchOnesAndUnitDigits(int number)
        {
                unitNumber = number%10;
                tensNumber = number/10;
            
        }

        public int AddSum()
        {
            int sum = tensNumber + unitNumber;
             Console.WriteLine("Addition of two digit number :"+ sum);
            return sum;
        }

        public int Subtract(int number,int sumObtained)
        {
            int newSum = number - sumObtained;
            Console.WriteLine("Difference of  Original number and added number :" + newSum);
            return newSum;
        }

    }
}
