﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DecagonProblem5
{
    internal class DateValidator
    {
        private DateTime date;
        private string inputDate;
        private string userInput;

        public void ValidateDate()
        {
            do
            {
                Console.WriteLine("Enter date in DD-MM-YYYY format");
                inputDate = Console.ReadLine();

                if (DateTime.TryParse(inputDate, out date))
                {
                    String.Format("{0:DD/MM/YYYY}", date);
                    Console.WriteLine("Valid Date Entered");
                }
                else
                {
                    Console.WriteLine("Invalid Date Entered");

                }
                Console.WriteLine("Want to try again?say yes or no");
                userInput = Console.ReadLine().ToLower();
            } while (userInput == "yes");

            Console.WriteLine("Thank you");
            Console.ReadLine();
        }
    }
}
