﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DecagonProblem7
{
    class LargestDivisor
    {
       

        public void GetDivisor()
        {
          int inputNumber;

             Console.WriteLine("Enter a three digit number");
            inputNumber = Convert.ToInt32(Console.ReadLine()); 
             double count = Math.Ceiling(Math.Log10(inputNumber));

          
                Console.WriteLine("The Greatest Divisor is : ");
                List<int> factorsList = new List<int>();
                for (int x = 1; x <= inputNumber; x++)
                {

                    if (inputNumber % x == 0)
                    {
                        // Console.WriteLine(x);
                        factorsList.Add(x);

                    }

                }

                int secondHighest = (from number in factorsList
                                     orderby number descending
                                     select number).Distinct().Skip(1).First();

            Console.WriteLine(secondHighest);
            Console.ReadLine();
        }

   
    }
}
