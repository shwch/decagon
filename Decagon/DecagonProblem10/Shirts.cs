﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace DecagonProblem10
{
    class Shirts
    {
       public float CalCloth()
        {
            const int cloth = 6;
            int typeOfShirts=0;

            Console.WriteLine("Enter types of shirt required");
            typeOfShirts = Convert.ToInt32(Console.ReadLine());
            
            float[] arrSum = new float[typeOfShirts];

            for ( int i =1 ; i<=typeOfShirts ; i++)
            {
                Console.Write("Enter cloth required for type " + i + ":");
                float requiredCloth = float.Parse(Console.ReadLine());

                Console.Write("Enter quantity of shirts required for type:" + i+":");
                int quantity = Convert.ToInt32(Console.ReadLine());
                arrSum[i-1] = (cloth/typeOfShirts) - (requiredCloth * quantity);

            }
            float remainingCloth = arrSum.Sum();
            return remainingCloth;
        }
    }
}
